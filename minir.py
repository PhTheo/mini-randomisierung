#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" participants are randomly allocated to assignments """

import random                 # library with helper functions

n = 10                        # number of students
participants = list(range(n)) # numbers from 0 to n, just to indicate n students
                                # should be replaced by a list of names

key = "495809djfalkd"         # key to make randomisation reproducible


print(participants)

# shuffle participants
random.seed(key)
random.shuffle(participants)

print(participants)
