#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" helper program, creates a csv file with some names """

import csv, names, random

n = 60

## generate a set of n fake names and matrikulation numbers 
participants = [ (names.get_full_name(), f"1111{i:04}" )
                 for i in random.sample(range(1000), n) ]

## write them to a csv file
with open('participants.csv', 'w') as csvfile:
    csvwriter = csv.writer(csvfile)
    for p in participants:
       csvwriter.writerow(p)
       
