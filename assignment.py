#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" participants are randomly allocated to assignments """

import csv, random

from itertools import cycle

key = "495809djfalkd"         # key to make randomisation reproducible (and transparent)
assignments = 30

## get list of participants
participants = []               # empty list
with open('participants.csv') as csvfile:
    csvreader = csv.reader(csvfile)
    for _, matno in csvreader:
        participants.append(matno)

## shuffle participants
random.seed(key)
random.shuffle(participants)

## couple with an assignment
participants = list(zip(participants, cycle(range(assignments))))

## Output
for (matno, assignment) in sorted(participants):
    print(f"{matno:10}: {assignment:4}")
    
