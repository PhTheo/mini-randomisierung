# Mini Randomisierung

Participants are randomly allocated to assignments

## Problem

Die $`n`$ Mitglieder eines Kurses sollen zufällig auf $`m`$ Gruppen
verteilt werden. Jede Gruppe bekommt dann ein Thema zu bearbeiten.

## Lösungsansatz

In Python kann das Problem elegant mit der Funktion
[`random.shuffle()`](https://docs.python.org/3/library/random.html)
gelöst werden.  Sie gibt die Elemente einer Liste (z. B. Namen oder
Matrikelnummern von Studierenden) in zufälliger Reihenfolge wieder.

Die Reihenfolge wird durch den
[*Randomisierungsschlüssel*](https://www.ncbi.nlm.nih.gov/books/NBK305495/)
`key` bestimmt.

Aus der Liste der Buchstaben von `A` bis `L` wird so z. B.
```python
p = [ chr(ord('A') + i) for i in range(15) ]
key = "test"
random.seed(key)
random.shuffle(p)

['G', 'J', 'K', 'N', 'C', 'D', 'L', 'M', 'I', 'O', 'A', 'E', 'B', 'H', 'F']
```

Diesen 12 Teilnehmern kann nun ein Satz von z. B. 5 Aufgaben zugeordnet werden:

```python
a = [1, 2, 3, 4, 5]
l = zip(p, cycle(a))

[('G', 1), ('J', 2), ('K', 3), ('N', 4), ('C', 5), ('D', 1), ('L', 2), ('M', 3), ('I', 4), ('O', 5), ('A', 1), ('E', 2), ('B', 3), ('H', 4), ('F', 5)]
```

oder sortiert:

```
sorted(l)

[('A', 1), ('B', 3), ('C', 5), ('D', 1), ('E', 2), ('F', 5), ('G', 1), ('H', 4), ('I', 4), ('J', 2), ('K', 3), ('L', 2), ('M', 3), ('N', 4), ('O', 5)]
``` 

## Programme

### assignment.py

Liste eine
[CSV](https://en.wikipedia.org/wiki/Comma-separated_values)-Datei mit
$`n`$ Namen und Matrikelnummern. Nach dem oben beschriebenen Verfahren
werden den Namen 30 Themennummern zugeordnet.

Ausgabe:

| Matrikelnummer | Thema |
| -------------- | -----:|
| 11110015       |    3  |
| 11110031       |    5  |
| 11110034       |    3  |
| 11110055       |    7  |
| 11110069       |   25  |
| 11110076       |   10  |
| 11110115       |   14  |
| 11110136       |   12  |
| 11110142       |   11  |
| 11110192       |    1  |
| 11110203       |   28  |
| 11110209       |   27  |
| 11110211       |   20  |
| 11110215       |    8  |
| 11110228       |   21  |
| 11110234       |    0  |
| 11110240       |   26  |
| 11110269       |   29  |
| 11110292       |   23  |
| 11110327       |   17  |
| 11110363       |   22  |
| 11110385       |   27  |
| 11110471       |    5  |
| 11110476       |    9  |
| 11110506       |    4  |
| 11110513       |   28  |
| 11110519       |   23  |
| 11110536       |   16  |
| 11110542       |   22  |
| 11110547       |   18  |
| 11110561       |   24  |
| 11110616       |    8  |
| 11110620       |    0  |
| 11110627       |    6  |
| 11110634       |   25  |
| 11110635       |   13  |
| 11110681       |   14  |
| 11110695       |    6  |
| 11110702       |   26  |
| 11110734       |   13  |
| 11110736       |   17  |
| 11110755       |   24  |
| 11110781       |   15  |
| 11110782       |   21  |
| 11110796       |   16  |
| 11110810       |   19  |
| 11110811       |    4  |
| 11110812       |   19  |
| 11110815       |   10  |
| 11110849       |   12  |
| 11110857       |    1  |
| 11110879       |   15  |
| 11110880       |    9  |
| 11110886       |    2  |
| 11110921       |   29  |
| 11110922       |    2  |
| 11110935       |    7  |
| 11110968       |   11  |
| 11110983       |   18  |
| 11110988       |   20  |


## Ausblick


### Teilnehmerliste

Die Namen und Matrikelnummern könnten auch direkt aus einer
[ILIAS](https://www.ilias.de/)-generierten Teilnehmerliste im
[HTML](https://en.wikipedia.org/wiki/HTML)-Format gelesen werden.


### Randomisierungschlüssel

Der Randomisierungsschlüssel ist momentan fest in `assignemnt.py`
vorgegeben. Er könnte auch (zusammen mit der Anzahl der Seminarthemen)
über die *Kommandozeile* oder aus einer *Konfigurationdatei* gelesen
werden.
